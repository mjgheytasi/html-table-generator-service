<div class="dataTables_info" id="example_info" role="status" aria-live="polite">
    Showing {{ $paginator->firstItem() }} to {{ $paginator->lastItem() }} of {{ $paginator->total() }} entries
</div>
